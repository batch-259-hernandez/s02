<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S02: Activity</title>
</head>

<body>
    <h1>Divisible of Five</h1>
    <p> <?php echo printDivisibleOfFive(); ?> </p>

    <h1>Array Manipuation</h1>
    <?php array_push($students, 'John Smith'); ?>
    <p> <?php echo var_dump($students); ?> </p>

    <pre> <?php echo count($students); ?> </pre>

    <?php array_push($students, 'Jane Smith'); ?>
    <p> <?php echo var_dump($students); ?> </p>

    <pre> <?php echo count($students); ?> </pre>

    <?php array_shift($students); ?>
    <p> <?php echo var_dump($students); ?> </p>

    <pre> <?php echo count($students); ?> </pre>






</body>

</html>